import React, { useState } from 'react';
import logo from './logo.svg';
import './App.css';
import SuperInputText from './c1-SuperInputText/SuperInputText';
import SuperButton from './c2-SuperButton/SuperButton';

function App() {
  const [arlan, setArlan] = useState("");
  const [abzal, setAbzal] = useState("");
  const [marik, setMarik] = useState("");
  const [cristine, setCristine] = useState("");
  const nameArr = ["Arlan", "Abzal", "Marik", "Christine"]
  const average = (a: number, b: number, c: number, d: number) => {
    return (a + b + c + d) / 4
  }
  const arr: number[] = []
  const fullArr = (num: number, avrNum: number) => {
    arr.push(num - avrNum)
  }


  const expenses = () => {

    for (let i = 0; arr.length > i; i++) {
      let j = 0;
      if (arr[i] > 0) {
        while (arr[i] > 0) {
          if (arr[j] < 0) {
            if (arr[i] + arr[j] >= 0) {
              arr[i] = arr[i] + arr[j];
              console.log(`${nameArr[j]} отправил(-а) к ${nameArr[i]} эту сумму: ${arr[j]} руб`);
              arr[j] = 0;
            }
            else {
              arr[i] = arr[i] + (arr[j] - (arr[j] - arr[i]))
              console.log(`${nameArr[j]} отправил к ${nameArr[i]} : ${(arr[j] - (arr[j] - arr[i]))}`);
              arr[j] = arr[j] - arr[i]
            }

          }
          j++
        }

      }
    }
  }



  const onClickHandler = () => {
    const averNum = average(parseInt(arlan), parseInt(abzal), parseInt(marik), parseInt(cristine));
    fullArr(parseInt(arlan), averNum);
    fullArr(parseInt(abzal), averNum);
    fullArr(parseInt(marik), averNum);
    fullArr(parseInt(cristine), averNum);
    expenses();


  }




  return (
    <div className="App">
      <label>
        Arlan   <SuperInputText onChangeText={setArlan} />
      </label>
      <label>
        Abzal <SuperInputText onChangeText={setAbzal} />
      </label>
      <label>
        Marik <SuperInputText onChangeText={setMarik} />
      </label>
      <label>
        Cristine <SuperInputText onChangeText={setCristine} />
      </label>


      <SuperButton onClick={onClickHandler}>count</SuperButton>
    </div>
  );
}

export default App;
